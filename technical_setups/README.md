
Installazione:
    - cavi JACK XLR         4           20m
    - pc permanente         1
    - monitor video         2
    - Genelec 1030A         4       (qui)[https://www.genelec.com/previous-models/1030a]

Concerto:
Attrezzatura in serie seguendo la catena elettroacustica
    - Aste microfoniche     8
    - mic cm4               2
    - radio mic             2
    - Jack - XLR            12
    - RME UFX               1
    - RME Fireface 800      1
    - Mac                   1
    - Adat                  2
    - Mixer DM1000          1
    - Speakon               2           10m
    - D&B                   8
    - D&B SUB               2
    - subwoofer Genelec     1
    - aste altoparlanti     2


Corrente:
    - ciabatte              3
    - ruzzola               2           25m

