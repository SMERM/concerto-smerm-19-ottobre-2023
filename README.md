# Repository per l'organizzazione del concerto della Scuola di Musica Elettronica del Conservatorio Statale di Musica di Roma, Santa Cecilia del 1mo marzo 2024

## Informazioni utili sul concerto

>Il concerto si terrà il 1mo marzo 2024 nella Sala Accademica del Conservatorio Statale di Musica di Roma, Santa Cecilia alle ore 18.


## Fasi di lavorazione

- [x] ricerca dei brani degli studenti ed ex studenti entro il 5 gennaio 2024
- [x] ricerca dei brani di docenti, ex docenti e compositori della Scuola Romana entro il 5 gennaio 2024
- [x] conclusione della caccia ai brani 12 gennaio 2024
- [x] consegna e visione delle schede tecniche e degli organici entro il 12 gennaio 2024
- [x] consegna dei brani 1mo febbraio 2024
- giorni di prove in aula 10,15,24,29 febbraio
- [ ] preparazione del materiale per i social entro il 10 febbraio 2024 (prima generico e poi sempre più dettagliato)
- [ ] inizio della campagna di pubblicizzazione del concerto dal 10 febbraio 2024
- [ ] organizzazione ed ascolto della scaletta provvisoria entro il 10 febbraio 2024, sentendo tutti i brani per la decisione della scaletta
- [ ] lista del materiale utile entro il 10 febbraio 2024
- [ ] preparazione materiale definitivo per il concerto entro il 15 febbraio 2024
- [ ] ritiro materiale informativo per il concerto entro il 27 febbraio 2024


## Materiali utili per il concerto (entro il 10 febbraio)

- [x] acusmatici da suonare
- [ ] lista dei materiali tecnici da utilizzare
- [x] partiture
- [x] biografie degli autori
- [x] 3 righe di introduzione al brano scritte dai compositori
- [x] lista degli esecutori

## Altri materiali (entro il 24 febbraio)
- [x] poster
- [x] programmi di sala
- [x] materiale esplicativo installazione
- [x] inviti ad ospiti

## Materiale pubblicitario da pubblicare
Piattaforme e servizi scelti:
- Instagram
- Facebook
- X
- Sito web Conservatorio
- Mailing list

## Calendario prove

- 10 febbraio (10-19) - Aula 1: ascolto e lettura dei brani e proposte di scaletta
- 15 febbraio (14-19) - Aula 1: ascolto con la scaletta finita
- 24 febbraio (10-15) - Aula 1: prova e registrazioni, preparazione alla prova generale 27 febbraio
- 27 febbraio (8-18) - Sala Accademica: prova generale
  - mattina: montaggio e prove tecniche, e prove di qualche brano con strumenti:
     - prove del brano di Alessandro Malcangi
     - prove del brano di Giovanni Michelangelo D'Urso
  - pomeriggio: prova generale
    - ordine della scaletta tranne i brani già provati la mattina
- 29 febbraio (14-19) - Aula 1: briefing pre-concerto
- 1mo marzo (15-18) - Sala Accademica: prove
- 1mo marzo (concerto ore 18) - Sala Accademica: concerto

## Disponibilità Esecutori

- 24 febbraio:
  - prova Giovanni Michelangelo D'Urso con eseuctori
  - prova Davide Tedesco con Elena D'Alò
  - sopralluogo Luca Spanedda
- 27 febbraio:
  - solo mattina:
    - Alessandro Malcangi
  - dalle 15 alle 16:40:
    - Elena D'Alò
## Brani del concerto

### Brani di studenti ed ex studenti SMERM

| Compositore    | Nome del brano | Organico                | Interpreti | Durata    | Richieste del Compositore | Links |
| :------------- | :------------- | :---------------------- |:--------- | :-------- | :----- | :------ |
| Giulio Romano De Mattia  | Aedi - AEDI - Transience   | Clarinetto Basso e Fixed Media  | Clarinettista: Alice Cortegiani, Live Electronics: Giulio Romano De Mattia| 5'20''    |? | |
| Giovanni Michelangelo D'Urso | Cronistoria di Un Viaggiatore Sedentario (trascrizione per chitarra elettrica, pianoforte, percussioni e paesaggi sonori (fixed media)) | Chitarra elettrica, Pianoforte, Percussioni e Paesaggi sonori (Fixed Media)  | Francesco Bianco (chitarra elettrica), Alessandro Malcangi (pianoforte), Berardo Di Mattia (percussioni; Setup percussioni: tam, una coppia di bongos, rullante, cassa, ride, crash), Davide Tedesco (esecuzione Fixed Media)| 10' | Diffusione paesaggi sonori: sistema ottofonico della sala accademica. Necessità di avere 3 piccoli schermi per i musicisti per avere un riferimento in secondi sui paesaggi sonori ed un ascolto in cuffia per Berardo Di Mattia. Non si prevede l'amplificazione degli strumenti.| |
| Davide Tedesco | Studio sul Corpo d'Ombra \#1 (2023) | Flauto Basso e Live Electronics | Elena D'Alò (Flauto) Live Electronics (da trovare)) | 7' | Primo o ultimo brano, data la presenza dello STONE da spostare dietro all'esecutore. | https://gitlab.com/DavideTedesco/Studio_sul_Corpo_dOmbra_Numero_1 |
| Alessandro Malcangi | Mutazioni in clessidra | Sax Contralto, Flauto, Clarinetto in Si b e Fixed Media| Alessandro Malcangi ed esecutore del Fixed Media  (da trovare) | 7' | Microfonazione adeguata per l'amplificazione di tutti gli strumenti.|
| Francesco Vesprini | Symphono | Acusmatico con video | Francesco Vesprini | 5' | - | -|
| Filippo Fossà | La Scogliera| Acusmatico | Filippo Fossà  | 6' | -| -|
| Francesco Ferracuti |Studio bicamerale n.1 | Flauto e Live Electronics | Alessandro Malcangi e Francesco Ferracuti  | 4' | -| -|

### Brani di docenti SMERM, ex docenti SMERM e compositori della Scuola Romana

| Compositore    | Nome del brano | Organico                | Interpreti | Durata    | Richieste del Compositore o indicazioni| Links |
| :------------- | :------------- | :---------------------- |:--------- | :-------- | :----- | :----- |
| Nicola Bernardini  | Recordare - Madrigale recitato per suoni elaborati (1999-2000) | Acusmatico  | Giulio Romano De Mattia | 6'55''  | Parte video del brano con testo che scorre per far comprendere alcune parti del brano (forse). Concentrarsi sulla diffusione. Isolata la parte di spazializzazione per poterlo riconfigurare per qualsiasi spazio. Spazializzazione frontale con spostamenti oscillatori destra/sinistra. _Da inserire in mezzo._ | https://gitlab.com/nicb/Recordare |
| Franco Evangelisti | Incontri di Fasce Sonore (1956-1957) | Acusmatico | da trovare | 3'23'' | _Brano in apertura._ ||

### Installazione

| Compositore    | Nome dell'installazione |Richieste del Compositore o indicazioni             | Links |
| :------------- | :------------- | :---------------------- | :-------- |
|Luca Spanedda, Dario Sanfilippo, Alan Alpenfelt e Daniela Allocca |Waste Kompost Radio || |
